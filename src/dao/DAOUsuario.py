import pymysql

class DAOUsuario:
    def connect(self):
        #return pymysql.connect("localhost","root","","db_final")
        return pymysql.connect(host="localhost", port=3307, user="root", passwd="", db="libreria")

    def read(self,id):
        con = DAOUsuario.connect(self)
        cursor = con.cursor()

        try:
            if id == None:
                cursor.execute("SELECT id_libro,nombre_genero,titulo,nombre_autor FROM tbllibro AS l JOIN genero AS g ON l.cod_genero = g.id_genero RIGHT JOIN tblautor AS a ON l.cod_autor = a.id_autor order by id_libro")
            else:
                cursor.execute("select * from tbllibro where id_libro = %s order by id_libro",(id,))
            return cursor.fetchall()
        except:
            return ()
        finally:
            con.close()
    
    def insert(self,data):
        con = DAOUsuario.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("INSERT INTO tbllector (id_lector,nombre_lector,direccion,telefono,email) values (%s,%s,%s,%s,%s)",(data['id_lector'],data['nombre_lector'],data['direccion'],data['telefono'],data['email']))
            con.commit()
            return True
        except:
            con.rollback()
            return False
        finally:
            con.close()
    
    def prestame(self,data):
        con = DAOUsuario.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("INSERT INTO tblprestamo (cod_lector,cod_libro,fecha_prestamo,fecha_devolucion,dias_prestamo) values (%s,%s,%s,%s,%s)",(data['cod_lector'],data['cod_libro'],data['fecha_prestamo'],data['fecha_devolucion'],data['dias_prestamo']))
            con.commit()
            return True
        except:
            con.rollback()
            return False
        finally:
            con.close()



    def prestamos(self,id):
        con = DAOUsuario.connect(self)
        cursor = con.cursor()

        try:
            if id == None:
                cursor.execute("SELECT id_prestamo,cod_libro,cod_lector,fecha_devolucion FROM tblprestamo")
            else:
                cursor.execute("SELECT id_prestamo,cod_libro,cod_lector,fecha_devolucion FROM tblprestamo WHERE id_prestamo = %s ORDER BY fecha_devolucion",(id,))
            return cursor.fetchall()
        except:
            return ()
        finally:
            con.close()


    def delete(self,id):
        con = DAOUsuario.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("DELETE FROM tblprestamo WHERE id_prestamo = %s",(id,))
            con.commit()
            return True
        except:
            con.rollback()
            return False
        finally:
            con.close()

    
    def update(self,id,data):
        con = DAOUsuario.connect(self)
        cursor = con.cursor()

        try:
            cursor.execute("Update tblprestamo set cod_libro = %s, cod_lector = %s, fecha_devolucion = %s WHERE id_prestamo = %s", (data['cod_libro'], data['cod_lector'], data['fecha_devolucion'], id,))
            con.commit()
            return True
        except:
            con.rollback()
            return False
        finally:
            con.close()
