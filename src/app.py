from flask import Flask, flash, render_template, redirect, url_for, request, session
from dao.DAOUsuario import DAOUsuario

app = Flask(__name__)
app.secret_key = "Tecsup123"
db = DAOUsuario()

@app.route("/")
def inicio():
    return render_template('index.html')

@app.route("/usuario/")
def index():
    data = db.read(None)

    return render_template('usuario/index.html', data = data)

@app.route("/usuario/lector/")
def infoLector():
    return render_template('/usuario/lector.html')

@app.route("/usuario/addlector/", methods=["POST","GET"])
def addlector():
     if request.method == "POST" and request.form['save']:
            if db.insert(request.form):
                flash('>Nuevo lector registrado correctamente')
            else:
                flash('ERROR en el registro del lector')
            return redirect(url_for('index'))
     else:
            return redirect(url_for('index'))

@app.route("/usuario/prestamo/")  
def prestamo():
    return render_template('/usuario/prestamo.html')

@app.route("/usuario/prestamisimo/", methods=["POST","GET"])
def prestamisimo():
    if request.method == "POST" and request.form['save']:
            if db.prestame(request.form):
                flash('>Prestamo satisfactorio')
            else:
                flash('ERROR en procesar prestamo')
            return redirect(url_for('index'))
    else:
            return redirect(url_for('index'))
    

@app.route("/usuario/prestamos/")
def prestamos():
    data = db.prestamos(None)

    return render_template('usuario/prestamos.html', data = data)


@app.route("/usuario/delete/<int:id>")
def delete(id):
    data = db.prestamos(id)
    if len(data) ==0:
        return redirect(url_for('index'))
    else:
        session['delete']=id
        return render_template('usuario/delete.html',data=data)
@app.route("/usuario/deleteusuario/" ,methods=["POST"])
def deleteusuario():
    if request.method == "POST" and request.form['delete']:
        if db.delete(session['delete']):
            flash('Prestamo Eliminado')
        else:
            flash('ERROR en eliminar Prestamo')
        session.pop('delete',None)
        return redirect(url_for('index'))
    else:
        return redirect(url_for('index'))

@app.route("/usuario/update/<int:id>/")
def update(id):
    data = db.prestamos(id)
    if len(data)==0:
        return redirect(url_for('index'))
    else:
        session['update'] = id
        return render_template('usuario/update.html' ,data=data)

@app.route("/usuario/updateusuario/",methods=["POST"])
def updatausuario():
    if request.method == "POST" and request.form['update']:
        if db.update(session['update'],request.form):
            flash('Se actualizo correctamente')
        else:
            flash('Error en actualizar')
        session.pop('update',None)
        return redirect(url_for('index'))
    else:
        return redirect(url_for('index'))


if __name__ == "__main__":
    app.run(debug=True,port=3000, host="0.0.0.0")

